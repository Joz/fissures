# Fissures
This mod adds large ground cracks to the map generator of minetest-game.

![Screenshot](screenshot.png)
If not happy with the frequency of occurence, adapt `probability_fissures_in_chunk` to your needs.